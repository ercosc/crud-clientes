//seta todas as rotas da aplicação
app.config(function($routeProvider) {
    $routeProvider.when('/clientes', {
        templateUrl: "views/home.html",
        controller: 'ListCtrl'
      })
      .when('/novo-cliente', {
        templateUrl: "views/novoUsuario.html",
        controller: 'CadastroCtrl'
      })
      .when('/clientes/:id', {
        templateUrl: "views/cliente.html",
        controller: 'ShowCtrl'
      })
      .when('/login', {
        templateUrl: "views/login.html",
        controller: 'LoginController'
      })
      .otherwise({
      redirectTo: '/login'
    });
  });