var app = angular.module('app', ['ngRoute', 'ui.mask', 'ngResource', 'ngCookies']);

app.run(['$rootScope', '$location', '$cookieStore', '$http',
    function ($rootScope, $location, $cookieStore, $http) {
        // manter usuario atualizado
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirecionar para a pagina de login se tentar acessar pela url
            if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
                $location.path('/login');
            }
            // redirecionar para a pagina de clientes caso cliente comum queira acessar cadastro pela url
            if($location.path() === '/novo-usuario' && $rootScope.globals.currentUser.user == 'comum') {
                $location.path('/clientes')
            }
        });
    }])



