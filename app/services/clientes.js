app.service('clienteService', function ($resource) {
    //variavel de cliente para ser utilizada no escopo das páginas
    var _cliente = {
        nome: '',
        cep: '',
        cidade: '',
        estado: '',
        cpf: '',
        bairro: '',
        casa: '',
        cep: '',
        cidade: '',
        cpf: '',
        estado: '',
        nome: '',
        rua: '',
        fones: [],
        emails: []
    }

    //variavel de fones para ser usada no escopo das páginas
    var _fones = [{
        num: '',
        tipo: ''
    }]

    //tornando possível acessar as variáveis pelos outros controllers
    this.cliente = _cliente;
    this.fones = _fones;

    //variável api que é o path da aplicação backend
    var api = $resource('http://localhost:8080/api/webresources/clientes/:id', {}, {
        //criação do método atualizar utilizando HTTP PUT
        atualizar: {
            method: "PUT"
        }
    })

    //função de listar
    this.listar = function () {
        return api.query().$promise
    }

    //função de acessar um único cliente
    this.mostrar = function (id) {
        return api.get({ id: id }).$promise
    }

    //função de salvar ou atualizar um novo cliente
    this.salvar = function (cliente) {
        if (cliente.id) {
            return api.atualizar({ id: cliente.id }, cliente).$promise;
        } else {
            return api.save(cliente).$promise
        }
    }

    //função de deletar cliente
    this.excluir = function (cliente) {
        return api.delete({ id: cliente.id }).$promise
    }

})


