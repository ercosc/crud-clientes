app.controller('ListCtrl', function ($scope, $rootScope, $window, clienteService) {
    
    //limpa o escopo de cliente pela service sempre ao acessar a página de listar clientes.
    clienteService.cliente = {};

    //função de listar que roda sempre que a página é autalizada
    listar();

    //função executada ao carregar a página que lista todos os clientes
    function listar() {
        clienteService.listar().then(function(clientes) {
            $scope.clientes = clientes;
        });
            
    }

    //função executada ao clicar botão de cadastrar novo cliente
    $scope.novoCliente = function() {
        clienteService.cliente = {};
        $window.location.href = "#/novo-cliente"
    }

    //função executada ao clicar botão excluir cliente
    $scope.excluir = function(cliente) {
        clienteService.excluir(cliente).then(listar);
    }


    //função executada ao clicar botão editar cliente
    $scope.editar = function(cliente) {
        clienteService.cliente = angular.copy(cliente)
        $window.location.href = '#/novo-cliente';
    }

    //função executada ao clicar em mostrar cliente
    $scope.mostrar = function(cliente) {
        clienteService.cliente = angular.copy(cliente);
        $window.location.href = '#/clientes/' + cliente.id
    }

    //função de deslogar do sistema e voltar pra pagina de login
    $scope.logout = function() {
        $rootScope.globals.currentUser = {}
        $window.location.href = '#/login'
    }
});
