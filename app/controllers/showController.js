app.controller('ShowCtrl', function($scope, $routeParams, clienteService, $window) {

    //recebe o id que vem da url e mostra o cliente daquele id
    if($routeParams.id) {
        clienteService.mostrar($routeParams.id).then(function(cliente) {
            $scope.cliente = cliente;
        })
    } else {
        clienteService.cliente = {};
    }
    
    //função executada ao clicar botão voltar
    $scope.voltar = function() {
        clienteService.cliente = {};
        $window.location.href = '#/clientes'
    }
})