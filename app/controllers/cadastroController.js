app.controller('CadastroCtrl', function ($scope, clienteService, $window) {

    //setam o escopo de clientes e fones igual ao cliente e fones existentes na service
    $scope.cliente = clienteService.cliente
    $scope.fones = clienteService.fones

    //arrays usados para iteração na criação de novos campos para Fone e Email
    $scope.newFoneItem = [];
    $scope.newEmailItem = [];

    //seta os tipos de telefones para utilizar na tag select
    $scope.tipo = ['celular', 'residencial', 'comercial']


    //função executada ao salvar um cliente
    $scope.salvar = function (cliente) {
        $scope.cliente.fones = $scope.fones;
        clienteService.salvar(cliente).then(function () {
            clienteService.cliente = {};
            clienteService.fones = {};
            $scope.form.$setPristine();
            $window.location.href = "#/clientes";
        });
    }

    //função executada ao clicar em voltar
    $scope.voltar = function () {
        clienteService.cliente = {};
        $window.location.href = "#/clientes"
    }

    //função executada ao apertar o botão de criar um novo campo de fone
    $scope.campoFone = function () {
        $scope.newFoneItem.push({ num: '', type: '' });
    }

    //função executada ao apertar o botão de remover um campo de fone
    $scope.removerFone = function (id) {
        $scope.fones.splice(id, 1);
        angular.element(document.getElementById('fone' + id)).remove();
    }

    //função executada ao apertar o botão de criar um novo campo de email
    $scope.campoEmail = function () {
        $scope.newEmailItem.push({ email: '' });
    }

    //função executada ao apertar o botão de remover um campo de email
    $scope.removerEmail = function (id) {
        $scope.cliente.emails.splice(id, 1);
        angular.element(document.getElementById('email' + id)).remove();
    }
});