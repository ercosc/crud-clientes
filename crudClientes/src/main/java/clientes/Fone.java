package clientes;
public class Fone {
    private String num;
    private String tipo;

    public Fone() {
    }

    public Fone(String num, String tipo) {
        this.num = num;
        this.tipo = tipo;
    }
    
    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    
}
