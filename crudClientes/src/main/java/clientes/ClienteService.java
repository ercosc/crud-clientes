
package clientes;

import java.util.List;
import javax.inject.Inject;


public class ClienteService implements IClienteService{
    
    @Inject
    private IClienteDao clienteDao;

    @Override
    public List<Cliente> listar() {
        return clienteDao.listar();
    }

    @Override
    public boolean adcionar(Cliente cliente) {
        return clienteDao.adcionar(cliente);
    }

    @Override
    public Cliente exibir(Integer id) {
        return clienteDao.exibir(id);
    }

    @Override
    public boolean atualizar(Cliente cliente, Integer id) {
        return clienteDao.atualizar(cliente, id);
    }

    @Override
    public boolean deletar(Integer id) {
        return clienteDao.deletar(id);
    }
}
