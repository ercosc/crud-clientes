package clientes;

import java.util.List;
import javax.inject.Singleton;


public class ClienteDao implements IClienteDao{
    
    @Singleton
    private SimulatedDatabase db;
    
    public ClienteDao() {
        this.db = new SimulatedDatabase();
    }

    @Override
    public List<Cliente> listar() {
        return db.listar();
    }

    @Override
    public boolean adcionar(Cliente cliente) {
        return db.adicionar(cliente);
    }

    @Override
    public Cliente exibir(Integer id) {
        return db.exibir(id);
    }

    @Override
    public boolean atualizar(Cliente cliente, Integer id) {
        return db.atualizar(cliente, id);
    }

    @Override
    public boolean deletar(Integer id) {
        return db.deletar(id);
    }
    
}
