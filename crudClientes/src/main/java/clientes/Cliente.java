package clientes;

import java.util.ArrayList;

//modelo de cliente a ser salvo na base de dados
public class Cliente {

    private Integer id;
    private String nome;
    private String cpf;
    private String cep;
    private String rua;
    private String casa;
    private String bairro;
    private String cidade;
    private String estado;
    private String Complemento;
    private ArrayList<Fone> fones = new ArrayList<>();
    private ArrayList<String> emails = new ArrayList<>();

    public Cliente() {
    }

    public Cliente(Integer id, String nome, String cpf, String cep, String rua, String casa, String bairro, String cidade, String estado, String complemento
    ,ArrayList<Fone> fones, ArrayList<String> emails
    ) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.cep = cep;
        this.rua = rua;
        this.casa = casa;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.Complemento = complemento;
        this.fones = fones;
        this.emails = emails;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getCasa() {
        return casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<Fone> getFones() {
        return fones;
    }

    public void setFones(ArrayList<Fone> fones) {
        this.fones = fones;
    }

    public ArrayList<String> getEmails() {
        return emails;
    }

    public String getComplemento() {
        return Complemento;
    }

    public void setComplemento(String Complemento) {
        this.Complemento = Complemento;
    }

    public void setEmails(ArrayList<String> emails) {
        this.emails = emails;
    }

}
