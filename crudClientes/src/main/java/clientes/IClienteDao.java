/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientes;

import java.util.List;
import javax.inject.Singleton;

public interface IClienteDao {

    @Singleton
     SimulatedDatabase db = new SimulatedDatabase();

    public List<Cliente> listar();

    public boolean adcionar(Cliente Cliente);

    public Cliente exibir(Integer id);

    public boolean atualizar(Cliente cliente, Integer id);

    public boolean deletar(Integer id);
}
