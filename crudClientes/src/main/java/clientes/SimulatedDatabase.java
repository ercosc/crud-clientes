package clientes;

import java.util.ArrayList;
import java.util.List;

//base de dados simulada que salva os clientes em um array. os dados são perdidos ao resetar o server
public class SimulatedDatabase {

    //contador para id de cliente
    int contador;
    ArrayList<Cliente> clientes;

    public SimulatedDatabase() {
        clientes = new ArrayList<>();
        contador = 1;
    }

    //função que lista todos os clientes
    public List<Cliente> listar() {
        return clientes;
    }

    //função que adiciona um cliente
    public boolean adicionar(Cliente cliente) {
        cliente.setId(contador++);
        
        return clientes.add(cliente);
    }

    //função que mostra um cliente pelo id
    public Cliente exibir(Integer id) {
        for (Cliente c : clientes) {
            if (c.getId().equals(id)) {
                return c;
            }
        }
        return null;
    }

    //função que atualiza um cliente existente
    public boolean atualizar(Cliente cliente, Integer id) {
        for (Cliente c : clientes) {
            if (c.getId().equals(id)) {
                c.setNome(cliente.getNome());
                c.setCpf(cliente.getCpf());
                c.setCep(cliente.getCep());
                c.setRua(cliente.getRua());
                c.setCasa(cliente.getCasa());
                c.setBairro(cliente.getBairro());
                c.setCidade(cliente.getCidade());
                c.setEstado(cliente.getEstado());
                c.setComplemento(cliente.getComplemento());
                c.setFones(cliente.getFones());
                c.setEmails(cliente.getEmails());
                return true;
            }
        }
        return false;
    }

    //função que deleta um cliente existente
    public boolean deletar(Integer id) {
        for (Cliente c : clientes) {
            if (c.getId().equals(id)) {
                clientes.remove(c);
                return true;
            }
        }
        return false;
    }
}
