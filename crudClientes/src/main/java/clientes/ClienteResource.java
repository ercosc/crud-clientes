package clientes;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("clientes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClienteResource {

    @Inject
    private IClienteService clienteService;

    //HTTP GET que lista todos os clientes acessando a base de dados simulada
    @GET
    public Response listar() {
        return Response.ok(clienteService.listar()).build();
    }

    //HTTP GET que mostra um cliente com base no id acessando a base de dados simulada
    @GET
    @Path("{id}")
    public Response exibir(@PathParam("id") Integer id) {
        return Response.ok(clienteService.exibir(id)).build();
    }

    //HTTP POST que adiciona um cliente na base de dados simulada
    @POST
    public Response adicionar(Cliente cliente) {
        return Response.ok(clienteService.adcionar(cliente)).build();
    }

    //HTTP PUT que atualiza um cliente acessando a base de dados simulada
    @PUT
    @Path("{id}")
    public Response atualizar(@PathParam("id") Integer id, Cliente cliente) {
        return Response.ok(clienteService.atualizar(cliente, id)).build();
    }

    //HTTP DELETE que deleta um cliente acessando a base de dados simulada
    @DELETE
    @Path("{id}")
    public Response deletar(@PathParam("id") Integer id) {
        return Response.ok(clienteService.deletar(id)).build();
    }
}
